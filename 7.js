var Number = function(num) {
  this.num = num;
};

Number.prototype.Sum = function(addValue) {
  this.value = addValue+this.num;
  this.num=this.value;
  return this;
};

Number.prototype.minus = function(minValue) {
  this.subValue = this.num-minValue;
  this.num=this.subValue;
  return this;
};
Number.prototype.expo = function(exp) {
  this.expValue =this.num*exp;
  this.num=this.expValue;
  return this;
};
Number.prototype.display = function() {
  console.log("The result of numeric operation is "+this.num);
};

new Number(5).Sum(7).minus(2).expo(2).display()