var gulp=require('gulp');
var less=require('gulp-less');
var sass=require('gulp-sass');
var util=require('gulp-util');
// var sourcemaps=require('gulp-sourcemaps')
gulp.task('sass',function(){
return gulp.src('./scss/*.scss')
	.pipe(less().on('error',util.log))
	.pipe(sass())
	.pipe(gulp.dest('./css/'));

});
gulp.task('watch',function(){
	gulp.watch('./scss/*.scss',['sass']);
})


// gulp.task('styles', function(){
//   return sass('./sass/*.scss',{
//     style: 'compressed'
//   })
//   .on('error', errorLog)
//   .pipe(autoprefixer(autoprefixerOptions))
//   .pipe(browserSync.stream())
//   .pipe(cssnano())
//   .pipe(gulp.dest('./css'))    /*Add '/' at the end of css */
// });