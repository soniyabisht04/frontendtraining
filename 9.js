var tQuery = function(str) {
  this.str = str;
  
};

tQuery.prototype.split = function(delimeter) {
  
  if(typeof(this.str)=="string"){
	  this.arr=this.str.split(delimeter);
	  
  }else{
		this.arr=[];
		for(var i=0;i<this.str.length;++i){
			var temp=this.str[i];
			this.gotArr=temp.split(delimeter);
			
			this.arr=this.arr.concat(this.gotArr);
		}
  }
  this.str=this.arr;
  return this;
};

tQuery.prototype.reverse = function() {
  
	
	this.arr=[];
	for(var i=0;i<this.str.length;++i){
		var temp=this.str[i];
		var newString = "";
		for (var j = temp.length - 1; j >= 0; j--) {
			newString += temp[j];
		}
		this.arr=this.arr.concat(newString);
		
	}
	
	
  
	this.str=this.arr;
	return this;
};
tQuery.prototype.join = function(delimeter) {
	
	
	this.myString=this.str.join(delimeter);
	this.str=[];
	this.str[0]=this.myString;
	
	return this;
};
tQuery.prototype.get = function() {
	
	console.log(this.str)
	return this;
};
new tQuery("12+23|45+67").split("|").reverse().split("+").join("-");